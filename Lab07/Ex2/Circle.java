/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Circle extends Shape {
	private double radius;
	
	public Circle() {
		super("circle");
	}
	public Circle(String _color, double _x, double _y) {
		super("circle", _color, _x, _y);
	}
	public Circle(String _color, double _x, double _y, double _radius) {
		super("circle", _color, _x, _y);
		radius = _radius;
	}
	
	public void setRadius(double _radius) {
		this.radius = _radius;
	}
	public double getRadius() {
		return this.radius;
	}
	
	public void show() {
		System.out.println(getRadius() + " " + super.getColor() + " " + super.getType() + "(" + super.getX() + "," + super.getY() + ")");
	}
}
