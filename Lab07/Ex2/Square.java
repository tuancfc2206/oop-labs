import exer1.Expression;

/*
 *
 * @author PhucPercy
 * @version 1.0 
 * @since 9/10/2018
 */

public class Square extends Shape {
	private double size;
	
	public Square() {
		super("square");
	}
	public Square(String _color, double _x, double _y) {
		super("square", _color, _x, _y);
	}
	public Square(String _color, double _x, double _y, double _size) {
		super("square", _color, _x, _y);
		size = _size;
	}
	
	public Square(Expression numeral1) {
		// TODO Auto-generated constructor stub
	}
	public void setSize(double _size) {
		this.size = _size;
	}
	public double getSize() {
		return this.size;
	}
	
	public void show() {
		System.out.println(getSize() + "x" + getSize() + " " + super.getColor() + " " + super.getType() + "(" + super.getX() + "," + super.getY() + ")");
	}
}
