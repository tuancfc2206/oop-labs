/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.util.*;

public class Layer {
	private ArrayList<Shape> shapeArr = new ArrayList<>();
	private boolean visible;
	
	public ArrayList<Shape> getLayer(){
		return this.shapeArr;
	}
	
	public void setVisible(boolean _visible) {
		visible = _visible;
	}
	public boolean getVisible() {
		return this.visible;  
	}
	
	/*
	 * delete triangle in layer
	 * @param nonce
	 * @return nonce
	 */
	public void delTriangle() {
		for (int i = 0; i < shapeArr.size(); ++i) {
			if(shapeArr.get(i).getType().equals("triangle"))	
				shapeArr.remove(i);
		}
	}
	
	/* 
	 * delete circle in layer 
	 * @param nonce
	 * @return nonce
	 */
	public void delCircle() {
		for (int i = 0; i < shapeArr.size(); ++i) {
			if(shapeArr.get(i).getType().equals("circle"))	
				shapeArr.remove(i);
		}
	}
	
	public void delDuplicate() {
		int i, j;
		
		for (i = 0; i < shapeArr.size(); ++i) {
			for (j = 0; j < i; ++j) {
				if (shapeArr.get(i).getType().equals(shapeArr.get(j).getType())){
					if (shapeArr.get(i).getType().equals("circle")) {
						shapeArr.remove(i);
					}
				}
				else if (shapeArr.get(i).getType().equals("trianle")) {
					
				}
			}
		}
	}
	
	/*
	 * show all shape in layer
	 * @param nonce
	 * @return nonce
	 */
	public void showAll() {
		for (int i = 0; i < shapeArr.size(); ++i) {
			shapeArr.get(i).show();
		}
	}
	
	/* 
	 * add shape in layer
	 * @param shape _shape
	 * @return nonce
	 */
	public void add(Shape _shape	) {
		shapeArr.add(_shape);
	}
}
