/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Ractangle extends Shape {
	private double width, height;
	
	public Ractangle() {
		super("ractangle");
	}
	public Ractangle(String _color, double _x, double _y) {
		super("ractangle", _color, _x, _y);
	}
	public Ractangle(String _color, double _x, double _y, double _width, double _height) {
		super("ractangle", _color, _x, _y);
		width = _width;
		height = _height;
	}
	
	public void setWidth(double _width) {
		this.width = _width;
	}
	public double getWidth() {
		return this.width;
	}
	
	public void setHeight(double _height) {
		this.height = _height;
	}
	public double getHeight() {
		return this.height;
	}

	public void show() {
		System.out.println(getHeight() + "x" + getWidth() + super.getColor() + " " + super.getType() + "(" + super.getX() + "," + super.getY() + ")");
	}
}

