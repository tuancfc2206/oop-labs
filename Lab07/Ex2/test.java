/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class test {
	public static void main(String[] args) {
		Diagram diagram = new Diagram();
		Shape shape1 = new Ractangle("blue", 1, 5, 6, 9);
		Shape shape2 = new Triangle("yellow", 3, 4, 3, 4, 5);
		Shape shape3 = new Circle("red", 2, 6, 7);
		Layer layer1 = new Layer();
		
		layer1.add(shape1);
		layer1.add(shape2);
		layer1.add(shape3);
		
		diagram.add(layer1);
		diagram.showAll();
		diagram.delCircle();
		diagram.showAll();
	}
}
