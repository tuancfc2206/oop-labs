/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.util.*;

public class Diagram {
	private ArrayList<Layer> layerArr = new ArrayList<>();
	
	public ArrayList<Layer> getDiagram(){
		return this.layerArr;
	}
	
	/* 
	 * delete circle in diagram 
	 * @param nonce
	 * @return nonce
	 */
	public void delCircle() {
		for (int i = 0; i < layerArr.size(); ++i) {
			layerArr.get(i).delCircle();
		}
	}
	
	/*
	 * add layer in diagram
	 * @param layer _layer
	 * @return nonce
	 */
	public void add(Layer _layer) {
		layerArr.add(_layer);
	}
	
	/*
	 * show all layer in diagram that are visible
	 * @param nonce
	 * @return nonce
	 */
	public void showAll() {
		for (int i = 0; i < layerArr.size(); ++i) {
			if (layerArr.get(i).getVisible())
				layerArr.get(i).showAll();
		}
	}
}
