/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.util.*;

public class Diagram {
    //private Layer[] layerList = new Layer[];
    private ArrayList<Layer> layerList = new ArrayList<>();
    public void deleteCircle() {
        for(int i = 0; i<layerList.size(); i++) {
            layerList.get(i).deleteCircle();
        }
    }
    public void add(Layer layer) {
        layerList.add(layer);
    }
    public void showAll() {
        for(int i = 0; i<layerList.size(); i++) {
            layerList.get(i).showAll();
        }
    }
}
