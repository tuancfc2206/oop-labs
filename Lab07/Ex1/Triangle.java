/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Triangle extends Shape {
    private double a, b, c;

    public Triangle() {
        super("triangle");
    }
    public Triangle(String Color, double x, double y) {
        super("triangle", Color, x, y);
    }
    public Triangle(String Color, double x, double y, double A, double B, double C) {
        super("triangle", Color, x, y);
        a = A;
        b = B;
        c = C;
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public double getC() {
        return c;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }

    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getA() + "x" + getB() + "x" + getC());
    }
}
