/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Rectangle extends Shape {
    private double w, h;

    public Rectangle() {
        super("rectangle");
    }
    public Rectangle(String Color, double x, double y) {
        super("rectangle", Color, x, y);
    }
    public Rectangle(String Color, double x, double y, double W, double H) {
        super("rectangle", Color, x, y);
        w = W;
        h = H;
    }

    public double getH() {
        return h;
    }
    public double getW() {
        return w;
    }

    public void setH(double h) {
        this.h = h;
    }

    public void setW(double w) {
        this.w = w;
    }

    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getH() + "x" + getW());
    }
}
