/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.util.*;

public class Layer {
    //private Shape[] shapeList = new Shape[];
    private ArrayList<Shape> shapeList = new ArrayList<>();
    public void deleteTriangle() {
        for(int i=shapeList.size()-1; i>=0; i--) {
            if(shapeList.get(i).getType() == "triangle") shapeList.remove(i);
        }
    }
    public void deleteCircle() {
        for(int i = shapeList.size()-1; i>=0; i--) {
            if(shapeList.get(i).getType() == "circle") shapeList.remove(i);
        }
    }
    public void showAll() {
        for(int i = 0; i<shapeList.size(); i++) {
            shapeList.get(i).show();
        }
    }
    public void add(Shape shape) {
        shapeList.add(shape);
    }

}
