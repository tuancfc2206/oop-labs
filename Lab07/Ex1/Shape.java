/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Shape {
    private String type;
    private String color;
    private double x, y;

    public Shape(String Type) {
        x = y = 0;
        type = Type;
        color = "white";
    }
    public Shape(String Type, String Color, double X, double Y) {
        type = Type;
        color = Color;
        x = X;
        y = Y;
    }

    public void setColor(String Color) {
        color = Color;
    }

    public void setType(String Type) {
        type = Type;
    }

    public void setX(double X) {
        x = X;
    }

    public void setY(double Y) {
        y = Y;
    }

    public String getType() {
        return type;
    }

    public String getColor() {
        return color;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public void move(int changeX, int changeY) {
        x += changeX;
        y += changeY;
    }

    public void show() {
    }
}
