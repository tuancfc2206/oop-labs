/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class test {
    public static void main(String[] args) {
        Diagram diagram = new Diagram();
        Shape shape1 = new Rectangle("red", 1, 1, 3, 5);
        Shape shape2 = new Circle("red", 1, 1, 3);
        Shape shape3 = new Circle("blue", 1, 1, 3);
        Layer layer1 = new Layer();
        layer1.add(shape1);
        layer1.add(shape2);
        layer1.add(shape3);
        diagram.add(layer1);
        diagram.showAll();
        diagram.deleteCircle();
        diagram.showAll();
    }
}
