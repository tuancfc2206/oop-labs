/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Circle extends Shape {
    private double r;

    public Circle() {
        super("circle");
    }
    public Circle(String Color, double x, double y) {
        super("circle", Color, x, y);
    }
    public Circle(String Color, double x, double y, double R) {
        super("circle", Color, x, y);
        r = R;
    }

    public double getR() {
        return r;
    }

    public void setR(double r) {
        this.r = r;
    }

    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getR());
    }
}
