/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Square extends Shape {
    private double size;

    public Square() {
        super("square");
    }
    public Square(String Color, double x, double y) {
        super("square", Color, x, y);
    }
    public Square(String Color, double x, double y, double Size) {
        super("square", Color, x, y);
        size = Size;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }
    public void show() {
        System.out.println(super.getType() + ":" + super.getColor() + "(" + super.getX() + ", " + super.getY() + ") " + getSize());
    }
}

