package fruit;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class HoaQua // Fruit
{
    protected String chungLoai;     // Type of Fruit
    protected String nguonGoc;      // Origin
    protected int giaBanTheoCan;    // Price per Kilogram (VND/kg)
    protected double khoiLuong;     // Mass (kg)

    /* Constructors */
    public HoaQua() {}

    public HoaQua(String chungLoai, String nguonGoc, int giaBanTheoCan, double khoiLuong)
    {
        this.chungLoai      = chungLoai;
        this.nguonGoc       = nguonGoc;
        this.giaBanTheoCan  = giaBanTheoCan;
        this.khoiLuong      = khoiLuong;
    }

    /* Getters and Setters */
    public String getChungLoai() {
        return chungLoai;
    }

    public void setChungLoai(String chungLoai) {
        this.chungLoai = chungLoai;
    }

    public String getNguonGoc() {
        return nguonGoc;
    }

    public void setNguonGoc(String nguonGoc) {
        this.nguonGoc = nguonGoc;
    }

    public int getGiaBanTheoCan() {
        return giaBanTheoCan;
    }

    public void setGiaBanTheoCan(int giaBanTheoCan) {
        this.giaBanTheoCan = giaBanTheoCan;
    }

    public double getKhoiLuong() {
        return khoiLuong;
    }

    public void setKhoiLuong(double khoiLuong) {
        this.khoiLuong = khoiLuong;
    }

    // Value (VND) = Mass (kg) * Price per Kilogram (VND/kg)
    public int giaTri()
    {
        return (int) (khoiLuong * giaBanTheoCan);
    }

    public void getDetail()
    {
        System.out.println("Chung loai: " + chungLoai);
        System.out.println("Nguon goc: " + nguonGoc);
        System.out.println("Gia ban theo can: " + giaBanTheoCan);
        System.out.println("Khoi luong: " + khoiLuong);
        System.out.println("Gia tri: " + giaTri());
    }

}