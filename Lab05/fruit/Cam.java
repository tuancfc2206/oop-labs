package fruit;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Cam extends HoaQua // Orange 
{
    private String chungLoaiCam;  // Type of Orange

    /* Constructors */
    public Cam()
    {
        setChungLoai("Cam");
    }

    public Cam(String chungLoaiCam, String nguonGoc, int giaBanTheoCan, double khoiLuong)
    {
        setChungLoai("Cam");
        this.chungLoaiCam   = chungLoaiCam;
        this.nguonGoc       = nguonGoc;
        this.giaBanTheoCan  = giaBanTheoCan;
        this.khoiLuong      = khoiLuong;
    }

    /* Getters and Setters */
    public String getChungLoaiCam() {
        return chungLoaiCam;
    }

    public void setChungLoaiCam(String chungLoaiCam) {
        this.chungLoaiCam = chungLoaiCam;
    }

    @Override
    public void getDetail()
    {
        System.out.println("Chung loai: " + chungLoai);
        System.out.println("Chung loai Cam: " + chungLoaiCam);
        System.out.println("Nguon goc: " + nguonGoc);
        System.out.println("Gia ban theo can: " + giaBanTheoCan);
        System.out.println("Khoi luong: " + khoiLuong);
        System.out.println("Gia tri: " + giaTri());
    }

}

