package fruit;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class CamCaoPhong extends Cam // Cao Phong Orange
{
    private double khuyenMai;   // Discount Percentage (%)

    /* Constructors */
    public CamCaoPhong() 
    {
        setNguonGoc("Cao Phong");
    }

    public CamCaoPhong(String chungLoaiCam, int giaBanTheoCan, double khuyenMai, double khoiLuong)
    {
        setNguonGoc("Cao Phong");
        setChungLoaiCam(chungLoaiCam);
        this.giaBanTheoCan  = giaBanTheoCan;
        this.khuyenMai      = khuyenMai;
        this.khoiLuong      = khoiLuong;
    }

    /* Getters and Setters */
    public double getKhuyenMai() {
        return khuyenMai;
    }

    public void setKhuyenMai(double khuyenMai) {
        this.khuyenMai = khuyenMai;
    }

    // Value (VND) = Mass (kg) * Price per Kilogram (VND/kg) - Discount
    public int giaTri()
    {
        return (int) (khoiLuong * giaBanTheoCan * (100 - khuyenMai) / 100);
    }

    @Override
    public void getDetail()
    {
        super.getDetail();
        System.out.println("<Khuyen mai " + khuyenMai + "%>");   
    }

}