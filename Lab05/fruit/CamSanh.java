package fruit;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class CamSanh extends Cam // King Orange
{
    private String mau = "Xanh"; // Color

    /* Constructors */
    public CamSanh()
    {
        setChungLoaiCam("Cam Sanh");
    }

    public CamSanh(String nguonGoc, String mau, int giaBanTheoCan, double khoiLuong)
    {
        setChungLoaiCam("Cam Sanh");
        this.nguonGoc       = nguonGoc;
        this.mau            = mau;
        this.giaBanTheoCan  = giaBanTheoCan;
        this.khoiLuong      = khoiLuong;
    }

    /* Getters and Setters */
    public String getMau() {
        return mau;
    }

    public void setMau(String mau) {
        this.mau = mau;
    }

    @Override
    public void getDetail()
    {
        System.out.println("Chung loai: " + chungLoai);
        System.out.println("Chung loai Cam: " + getChungLoaiCam());
        System.out.println("Mau: " + mau);
        System.out.println("Nguon goc: " + nguonGoc);
        System.out.println("Gia ban theo can: " + giaBanTheoCan);
        System.out.println("Khoi luong: " + khoiLuong);
        System.out.println("Gia tri: " + giaTri());
    }

}