package fruit;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

import fruit.Cam;
import fruit.HoaQua;

public class Tester
{
    public static void main(String[] args) {
        // Create New Object of Each Class
        HoaQua banana = new HoaQua("Chuoi", "Ha Noi", 20000, 2.5);
        Tao apple = new Tao("Tao Xanh", "Xanh", "My", 55555, 18);
        Cam orange = new Cam("Cam Vinh", "Vinh", 35000, 1000);
        CamCaoPhong caoPhongOrange = new CamCaoPhong("Cam Duong Canh", 40000, 10.5, 2.5);
        CamSanh kingOrange = new CamSanh("Bo Ha", "Vang Xanh", 20000, 5);

        // Convert Object of Subclass to Superclass
        HoaQua apple2 = apple;
        HoaQua orange2 = orange;
        HoaQua caoPhongOrange2 = caoPhongOrange;
        Cam kingOrage2 = kingOrange;
        
        // Printing Result
        banana.getDetail(); System.out.println();
        apple2.getDetail(); System.out.println();
        orange2.getDetail(); System.out.println();
        caoPhongOrange2.getDetail(); System.out.println();
        kingOrage2.getDetail(); System.out.println();
        
    }
}