package fruit;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Tao extends HoaQua // Apple
{
    private String chungLoaiTao;    // Type of Apple
    private String mau = "Do";      // Color

    /* Constructors */
    public Tao() 
    {
        setChungLoai("Tao");
    }

    public Tao(String chungLoaiTao, String mau, String nguonGoc, int giaBanTheoCan, double khoiLuong)
    {
        setChungLoai("Tao");
        this.chungLoaiTao   = chungLoaiTao;
        this.mau            = mau;
        this.nguonGoc       = nguonGoc;
        this.giaBanTheoCan  = giaBanTheoCan;
        this.khoiLuong      = khoiLuong;
    }

    /* Getters and Setters */
    public String getMau() {
        return mau;
    }

    public void setMau(String mau) {
        this.mau = mau;
    }

    public String getChungLoaiTao() {
        return chungLoaiTao;
    }

    public void setChungLoaiTao(String chungLoaiTao) {
        this.chungLoaiTao = chungLoaiTao;
    }

    @Override
    public void getDetail()
    {
        System.out.println("Chung loai: " + chungLoai);
        System.out.println("Chung loai Tao: " + chungLoaiTao);
        System.out.println("Nguon goc: " + nguonGoc);
        System.out.println("Gia ban theo can: " + giaBanTheoCan);
        System.out.println("Khoi luong: " + khoiLuong);
        System.out.println("Gia tri: " + giaTri());
    }

}

