package geometry;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Rectangle extends Shape
{
    private double width = 1.0;
    private double length = 1.0;

    /* Constructors */
    public Rectangle() {}

    public Rectangle(double width, double length)
    {
        this.width = width;
        this.length = length;
    }

    public Rectangle(double width, double length, String color, boolean filled)
    {
        this(width, length);
        setColor(color);
        setFilled(filled);
    }

    /* Getters and Setters */
    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    // S = a * b
    public double getArea()
    {
        return width * length;
    }

    // P = 2(a + b)
    public double getPerimeter()
    {
        return 2 * (width + length);
    }

    @Override
    public String toString() {
        return new String(getColor() + (isFilled() ? "-filled" : "-bordered") + " rectangle - P: " + getPerimeter() + " - S: " + getArea());
    }

}