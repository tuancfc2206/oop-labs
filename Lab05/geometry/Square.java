package geometry;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Square extends Rectangle
{
    /* Constructors */
    public Square() {}

    public Square(double side)
    {
        super(side, side);
    }

    public Square(double side, String color, boolean filled)
    {
        super(side, side, color, filled);
    }

    /* Getters and Setters */
    public double getSide()
    {
        return getWidth();
    }

    public void setSide(double side)
    {
        super.setWidth(side);
        super.setLength(side);
    }

    @Override
    public void setWidth(double side) {
        setSide(side);
    }

    @Override
    public void setLength(double side) {
        setSide(side);
    }

    @Override
    public String toString() {
        return new String(getColor() + (isFilled() ? "-filled" : "-bordered") + " square - P: " + getPerimeter() + " - S: " + getArea());
    }

}