package geometry;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Tester
{
    public static void main(String[] args) {
        // Create New Object of Each Class
        Shape shape = new Shape();
        Circle circle = new Circle(1, "blue", true);
        Rectangle rectangle = new Rectangle(1, 2);
        Square square = new Square(4, "green", false);

        // Print Result
        System.out.println(shape.toString());
        System.out.println(circle.toString());
        System.out.println(rectangle.toString());
        System.out.println(square.toString());
    }
}