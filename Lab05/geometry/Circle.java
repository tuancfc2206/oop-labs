package geometry;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

import java.util.Formatter;

public class Circle extends Shape
{
    private final static double PI = Math.PI;
    private double radius = 1.0;

    /* Constructors */
    public Circle() {}

    public Circle(double radius)
    {
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled)
    {
        super(color, filled);
        this.radius = radius;
    }

    /* Getters and Setters */
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // S = r*r*pi
    public double getArea()
    {
        return radius * radius * PI;
    }

    // P = 2r*pi
    public double getPerimeter()
    {
        return 2 * radius * PI;
    }

    @Override
    public String toString() {
        Formatter fmt = new Formatter();
        fmt.format("%.4f", getPerimeter());
        fmt.format("%.4f", getArea());
        fmt.close();
        return new String(getColor() + (isFilled() ? " round" : " circle") + " - P: " + fmtP.toString() + " - S: " + fmtS.toString());
    }

}