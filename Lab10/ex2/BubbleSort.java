package ex2;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.util.*;

public class BubbleSort {
	
	public static void bubbleSort(double[] arr, int n) {
		int i, j;
		double tmp;
		
		for(i = 0; i < n-1; ++i) {
			for (j = 0; j < n-i-1; ++j) {
				if (arr[j] > arr[j+1]) {
					tmp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = tmp;
				}
			}
		}
	}
	
	public static void main(String[] args) {
		int n = 1000;
		double[] arr = new double[n];
		Random random = new Random();
		
		for (int i = 0; i < n; ++i) {
			arr[i] = random.nextDouble();
		}
		
		for (int i = 0; i < n; ++i) {
			System.out.print(arr[i] + " | ");
		}
		System.out.println("\n ---------------------------");
		
		bubbleSort(arr, n);
		
		for (int i = 0; i < n; ++i) {
			System.out.print(arr[i] + " | ");
		}
	}
}
