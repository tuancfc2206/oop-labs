package ex1;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import ex.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Ultil {
	private static final File file = new File("Week_9/ex/Utils.java");
	
	/*
	 * dem so luong ngoac dong mo
	 * @param string s
	 * @return ngoac mo - ngoac dong
	 */
	public static int countBracket(String s) {
		int res = 0;
		int bracket = -1;
		
		while(bracket < s.length()) {
			bracket = s.indexOf('{', bracket + 1);
			if (bracket != -1)
				++res;
			else 
				break;
		}
		
		while(bracket < s.length()) {
			bracket = s.indexOf('}', bracket + 1);
			if (bracket != -1)
				--res;
			else 
				break;
		}
		
		return res;
	}
	
	/*
	 * lay ten cua tat ca cac ham
	 * @param ten trong phan khai bao ham va file
	 * @return  danh sach ten ham 
	 * @throws IOException
	 */
	public static List<String> getAllFunctionName(String name, File inFile) throws IOException {
		Utils utils = new Utils();
		String content = utils.readContentFromFile(inFile.getPath());
		List<String> list = new LinkedList<String> ();
		List<Integer> index = new LinkedList<Integer> ();
		content += '\n';
		int start = 0;
		int end;
		
		while (start < content.length()) {
			end = content.indexOf('\n',start);
            String tmp = content.substring(start,end);
            start = end+1;
            if(tmp.contains(name)) {
                String function = tmp+"\n";
                index.add(new Integer(start-tmp.length()));
                int dem = countBracket(tmp);
                if(!tmp.contains("{") || dem != 0){
                    while (start < content.length()){
                        end = content.indexOf('\n', start);
                        tmp = content.substring(start, end);
                        start = end + 1;
                        function += tmp + "\n";
                        dem += countBracket(tmp);
                        if(dem == 0) break;
                    }
                }
                list.add(function);
            }
		}
		
		return list;
	}
}
