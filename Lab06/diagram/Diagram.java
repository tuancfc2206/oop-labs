package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

import java.util.ArrayList;

public class Diagram
{
    private ArrayList<Layer> layers;

    public Diagram()
    {
        layers = new ArrayList<Layer>();
    }

    public ArrayList<Layer> getLayers() {
        return layers;
    }

    public void addLayer(Layer layer)
    {
        layers.add(layer);
    }

    public void removeCircles()
    {
        for(int i = 0, size = layers.size(); i < size; ++i)
        {
            ArrayList<Shape> shapes = layers.get(i).getShapes();
            for(int j = shapes.size() - 1; j >= 0; --j)
                if (shapes.get(j) instanceof Circle) shapes.remove(j);
        }
    }

}