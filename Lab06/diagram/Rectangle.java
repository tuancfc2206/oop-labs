package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Rectangle extends Shape
{
    private Point topLeft;
    private Point bottomRight;

    /* Constructors */
    public Rectangle() {}

    public Rectangle(Point topLeft, Point bottomRight)
    {
        this.topLeft = topLeft;
        this.bottomRight = bottomRight;
    }

    public Rectangle(Point topLeft, Point bottomRight, String color, boolean filled)
    {
        this(topLeft, bottomRight);
        setColor(color);
        setFilled(filled);
    }

    /* Getters and Setters */
    public Point getTopLeft() {
        return topLeft;
    }

    public void setTopLeft(Point topLeft) {
        this.topLeft = topLeft;
    }

    public Point getBottomRight() {
        return bottomRight;
    }

    public void setBottomRight(Point bottomRight) {
        this.bottomRight = bottomRight;
    }
    
    public double getHorizontalSide()
    {
        return topLeft.getX() - bottomRight.getX();
    }

    public double getVerticalSide()
    {
        return topLeft.getY() - bottomRight.getY();
    }

    public double getPerimeter()
    {
        return 2 * (getHorizontalSide() + getVerticalSide());
    }

    public double getArea()
    {
        return getHorizontalSide() * getVerticalSide();
    }

    public void move(Vector vector)
    {
        topLeft.move(vector);
        bottomRight.move(vector);
    }

}