package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

import java.util.ArrayList;

public class Layer
{
    private ArrayList<Shape> shapes;

    public Layer()
    {
        shapes = new ArrayList<Shape>();
    }

    public ArrayList<Shape> getShapes() {
        return shapes;
    }

    public void addShapes(Shape shape)
    {
        shapes.add(shape);
    }

    public void removeTriangles()
    {
        for(int i = shapes.size() - 1; i >= 0; --i)
            if (shapes.get(i) instanceof Triangle) shapes.remove(i);
    }

}