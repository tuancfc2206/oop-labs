package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

import java.util.Formatter;

public class Circle extends Shape
{
    private final static double PI = Math.PI;
    private Point center;
    private double radius = 1.0;

    /* Constructors */
    public Circle() {}

    public Circle(Point center, double radius)
    {
        this.center = center;
        this.radius = radius;
    }

    public Circle(Point center, double radius, String color, boolean filled)
    {
        super(color, filled);
        this.center = center;
        this.radius = radius;
    }

    /* Getters and Setters */
    public Point getCenter() {
        return center;
    }

    public void setCenter(Point center) {
        this.center = center;
    }

    public double getRadius()
    {
        return radius;
    }

    public void setRadius(double radius)
    {
        this.radius = radius;
    }

    public double getPerimeter()
    {
        return 2 * radius * PI;
    }

    public double getArea()
    {
        return radius * radius * PI;
    }

    public void move(Vector vector)
    {
        center.move(vector);
    }

}