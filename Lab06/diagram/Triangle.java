package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Triangle extends Shape
{
    private Point pointA, pointB, pointC;

    public Triangle() {}

    public Triangle(Point pointA, Point pointB, Point pointC)
    {
        this.pointA = pointA;
        this.pointB = pointB;
        this.pointC = pointC;
    }

    public Triangle(Point pointA, Point pointB, Point pointC, String color, boolean filled)
    {
        this(pointA, pointB, pointC);
        setColor(color);
        setFilled(filled);
    }

    public Point getPointA() {
        return pointA;
    }

    public void setPointA(Point pointA) {
        this.pointA = pointA;
    }

    public Point getPointB() {
        return pointB;
    }

    public void setPointB(Point pointB) {
        this.pointB = pointB;
    }

    public Point getPointC() {
        return pointC;
    }

    public void setPointC(Point pointC) {
        this.pointC = pointC;
    }

    public double getSideAB()
    {
        return pointA.distance(pointB);
    }

    public double getSideBC()
    {
        return pointB.distance(pointC);
    }

    public double getSideCA()
    {
        return pointC.distance(pointA);
    }

    public void move(Vector vector)
    {
        pointA.move(vector);
        pointB.move(vector);
        pointC.move(vector);
    }

}