package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

import java.util.ArrayList;

public class Tester
{
    public static void main(String[] args) {
        Shape c1 = new Circle(), c2 = new Circle(), c3 = new Circle();
        Shape t1 = new Triangle(), t2 = new Triangle(), t3 = new Triangle();
        Shape r1 = new Rectangle(), s1 = new Square();

        Layer l1 = new Layer();
        l1.addShapes(c1);
        l1.addShapes(t1);
        l1.addShapes(r1);
        
        Layer l2 = new Layer();
        l2.addShapes(c2);
        l2.addShapes(t2);
        l2.addShapes(s1);
        
        Layer l3 = new Layer();
        l3.addShapes(c3);
        l3.addShapes(t3);
        l3.addShapes(r1);

        Diagram diagram = new Diagram();
        diagram.addLayer(l1);
        diagram.addLayer(l2);
        diagram.addLayer(l3);

        l1.removeTriangles();
        diagram.removeCircles();
        
        for(int i = l1.getShapes().size() - 1; i >= 0; --i)
            if (l1.getShapes().get(i) instanceof Triangle)
            {
                System.out.println("Still remains Triangle");
                return;
            }

        for(int i = 0, size = diagram.getLayers().size(); i < size; ++i)
        {
            ArrayList<Shape> shapes = diagram.getLayers().get(i).getShapes();
            for(int j = shapes.size() - 1; j >= 0; --j)
                if (shapes.get(j) instanceof Circle)
                {
                    System.out.println("Still remains Circle");
                    return;
                }
        }

        System.out.println("OK");

    }
}