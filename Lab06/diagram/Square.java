package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Square extends Rectangle
{
    /* Constructors */
    public Square() {}

    public Square(Point topLeft, double side)
    {
        super(topLeft, new Point(topLeft.getX() + side,topLeft.getY() - side));
    }

    public Square(Point topLeft, double side, String color, boolean filled)
    {
        super(topLeft, new Point(topLeft.getX() + side,topLeft.getY() - side), color, filled);
    }

    /* Getters and Setters */
    public double getSide()
    {
        return getVerticalSide();
    }

}