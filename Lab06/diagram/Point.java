package diagram;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 30/9/2018
 */

public class Point
{
    private double x;
    private double y;

    public Point() {}

    public Point(double x, double y)
    {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void move(Vector vector)
    {
        x += vector.getX();
        y += vector.getY();
    }

    public double distance(Point another)
    {
        return Math.sqrt((x - another.x)*(x - another.x) + (y - another.y)*(y - another.y));
    }

}