/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Fraction implements Comparable<Fraction>
{
    double x, y;

    Fraction(double x, double y)
    {
        if (y < 0)
        {
            x = -x;
            y = -y;
        }
        this.x = x;
        this.y = y;
    }

    public int compareTo(Fraction t)
    {
        return (int) (x * t.y - y * t.x);
    }

    public String toString()
    {
        return x + "/" + y;
    }

}