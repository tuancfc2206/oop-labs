/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.util.ArrayList;

public class Main
{
    public static <T extends Comparable<T>> void sort(T[] a)
    {
        for(int i = 0, n = a.length - 1; i < n; ++i)
            for(int j = i + 1; j <= n; ++j)
                if (a[i].compareTo(a[j]) > 0)
                {
                    T temp;
                    temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
    }

    public static <T> void print(T[] a)
    {
        for(int i = 0, n = a.length; i < n; ++i)
            System.out.print(a[i].toString() + " ");
        System.out.println();
        System.out.println();
    }

    public static <T> ArrayList<T> toArrayList(T[] a)
    {
        ArrayList<T> array = new ArrayList<T>();
        for(int i = 0, n = a.length; i < n; ++i) array.add(a[i]);
        return array;
    }

    public static <T extends Comparable<T>> T maxElement(ArrayList<T> a)
    {
        T result = a.get(0);
        for(T x : a)
            if (result.compareTo(x) < 0) result = x;
        return result;
    }

    public static void main(String[] args)
    {
        Boolean[] booleans = {true, false, true, true, false};
        System.out.println(maxElement(toArrayList(booleans)));
        sort(booleans);
        print(booleans);

        Byte[] bytes = {5, 6, -1, 0, -1, -2, 4, 8};
        System.out.println(maxElement(toArrayList(bytes)));
        sort(bytes);
        print(bytes);

        Character[] characters = {'c', '<', 'b', 'z', '!', '?', 'H', 'G'};
        System.out.println(maxElement(toArrayList(characters)));
        sort(characters);
        print(characters);

        Short[] shorts = {5, 6, -1, 0, -1, -2, 4, 8};
        System.out.println(maxElement(toArrayList(shorts)));
        sort(shorts);
        print(shorts);

        Integer[] integers = {5, 6, -1, 0, -1, -2, 4, 8};
        System.out.println(maxElement(toArrayList(integers)));
        sort(integers);
        print(integers);

        Long[] longs = {5L, 6L, -1L, 0L, -1L, -2L, 4L, 193479108237492312L};
        System.out.println(maxElement(toArrayList(longs)));
        sort(longs);
        print(longs);
        
        Float[] floats = {3.14f, 0.00f, 2.71828182846f, -1.110f, 1234f};
        System.out.println(maxElement(toArrayList(floats)));
        sort(floats);
        print(floats);

        Double[] doubles = {0d, -1.234d, Math.PI, Math.E, Math.sqrt(100), -1e2, 5e-2, Math.exp(5)};
        System.out.println(maxElement(toArrayList(doubles)));
        sort(doubles);
        print(doubles);

        String[] strings = {"Hoang", "Khanh", "Do"};
        System.out.println(maxElement(toArrayList(strings)));
        sort(strings);
        print(strings);

        Fraction[] fractions = {new Fraction(3, 1), new Fraction(0, 4), new Fraction(-1, 5), new Fraction(7, 1)};
        System.out.println(maxElement(toArrayList(fractions)));
        sort(fractions);
        print(fractions);

    }
}