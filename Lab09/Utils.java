/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Utils
{
    public static String readContentFromFile(String path)
    {
        try
        {
            BufferedReader reader = new BufferedReader(new FileReader(new File(path)));
            String string = "";
            String temporary;
            while ((temporary = reader.readLine()) != null)
                string = string + "\n" + temporary;
            reader.close();
            return string;
        }
        catch (IOException exception)
        {
            System.out.println(exception.getMessage());
            return null;
        }
    }

    public static void writeContentToFile(String path, String string, boolean continuingWriting)
    {
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter(new File(path), continuingWriting));
            writer.write(string);
            writer.close();
        }
        catch (IOException exception)
        {
            System.out.println(exception.getMessage());
        }
    }

    public static File findFileByName(String folderPath, String fileName)
    {
        File file = new File(folderPath + "/" + fileName);
        if (file.exists())
        {
            if (file.isDirectory()) return null;
            return file;
        }
        return null;
    }

    public static void main(String[] args)
    {
        if (findFileByName("D:/Workspace/Workspace Java/Lab09", "text.txt") != null)
            System.out.println("File \"test.txt\" exists!");
        else
            System.out.println("File \"test.txt\" does not exist!");

        writeContentToFile("D:/Workspace/Workspace Java/Lab09/text.txt", "a", false);

        if (findFileByName("D:/Workspace/Workspace Java/Lab09", "text.txt") != null)
            System.out.println("File \"test.txt\" exists!");
        else
            System.out.println("File \"test.txt\" does not exist!");

        System.out.println();            

        System.out.println(readContentFromFile("D:/Workspace/Workspace Java/Lab09/text.txt"));
        System.out.println();

        writeContentToFile("D:/Workspace/Workspace Java/Lab09/text.txt", "b", false);

        System.out.println(readContentFromFile("D:/Workspace/Workspace Java/Lab09/text.txt"));
        System.out.println();

        writeContentToFile("D:/Workspace/Workspace Java/Lab09/text.txt", "\nc", true);

        System.out.println(readContentFromFile("D:/Workspace/Workspace Java/Lab09/text.txt"));
        System.out.println();

    }

}