package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

abstract class BinaryExpression extends Expression
{
    abstract Expression left();
    abstract Expression right();

}