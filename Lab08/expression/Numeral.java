package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Numeral extends Expression
{
    private int value;

    public Numeral()
    {
        this.value = 0;
    }

    public Numeral(int value)
    {
        this.value = value;
    }

    public String toString()
    {
        return value < 0 ? "(" + String.valueOf(value) + ")" : String.valueOf(value);
    }

    public int evaluate()
    {
        return value;
    }

}