package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

abstract class Expression
{
    @Override
    public abstract String toString();
    public abstract int evaluate();
    
}