package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Division extends Expression
{
    private Expression left;
    private Expression right;

    public Division(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }

    public String toString()
    {
        return (left instanceof Addition ||  left instanceof Subtraction ? "(" +  left.toString() + ")" :  left.toString()) + 
        "/" + (right instanceof Numeral ? right.toString() : "(" + right.toString() + ")");
    }

    public int evaluate()
    {
        return left.evaluate() / right.evaluate();
    }

}