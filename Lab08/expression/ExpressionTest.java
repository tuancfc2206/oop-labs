package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class ExpressionTest
{
    public static void main(String args[])
    {
        System.out.println("Case of (10^2 - 1 + 2*3)^2 :");

        try
        {
            Numeral n1 = new Numeral(1);
            Numeral n2 = new Numeral(2);
            Numeral n3 = new Numeral(3);
            Numeral n10 = new Numeral(10);

            Expression s1 = new Square(n10);
            Expression s2 = new Subtraction(s1, n1);
            Expression m1 = new Multiplication(n2, n3);
            Expression a1 = new Addition(s2, m1);

            Expression expression = new Square(a1);

            System.out.println(expression.toString() + " = " + expression.evaluate());
        }
        catch (ArithmeticException exception)
        {
            System.out.println("Loi chia cho 0");
        }

        System.out.println();
        
        System.out.println("Case of ((10^2 - 1)/(2 + (-2)))^2 :");
        try
        {
            Numeral n1 = new Numeral(1);
            Numeral n2 = new Numeral(2);
            Numeral n3 = new Numeral(-2);
            Numeral n10 = new Numeral(10);

            Expression s1 = new Square(n10);
            Expression s2 = new Subtraction(s1, n1);
            Expression m1 = new Addition(n2, n3);
            Expression a1 = new Division(s2, m1);

            Expression expression = new Square(a1);

            System.out.println(expression.toString() + " = " + expression.evaluate());
        }
        catch (ArithmeticException exception)
        {
            System.out.println("Loi chia cho 0");
        }
    }
}