package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Addition extends Expression
{
    private Expression left;
    private Expression right;

    public Addition(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }

    public String toString()
    {
        return left.toString() + " + " + right.toString();
    }

    public int evaluate()
    {
        return left.evaluate() + right.evaluate();
    }

}