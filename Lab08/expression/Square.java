package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Square extends Expression
{
    private Expression expression;

    public Square(Expression expression)
    {
        this.expression = expression;
    }

    public String toString()
    {
        return expression instanceof Numeral ? expression.toString() + "^2" : "(" + expression.toString() + ")^2";
    }

    public int evaluate()
    {
        int value = expression.evaluate();
        return value * value;
    }

}