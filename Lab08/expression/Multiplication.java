package expression;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

public class Multiplication extends Expression
{
    private Expression left;
    private Expression right;

    public Multiplication(Expression left, Expression right)
    {
        this.left = left;
        this.right = right;
    }

    public String toString()
    {
        return (left instanceof Addition ||  left instanceof Subtraction ? "(" +  left.toString() + ")" :  left.toString()) + 
        "*" + (right instanceof Addition || right instanceof Subtraction ? "(" + right.toString() + ")" : right.toString());
    }

    public int evaluate()
    {
        return left.evaluate() * right.evaluate();
    }

}