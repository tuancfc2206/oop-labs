package exception;

/**
 * @author Ha Minh Tuan
 * @version 1.8
 * @since 10/12/2018
 */

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class ExceptionTest
{
    public static void checkNullPointer(Object object)
    {
        try
        {
            System.out.print("Accessing " + object);
            object.toString();
            System.out.print(" does not trigger");
        }
        catch (NullPointerException exception)
        {
            System.out.print(" triggers");
        }
        finally
        {
            System.out.println(" NullPointerException");
        }
    }

    public static void checkIndexOutOfBounds(ArrayList<Integer> list, int index)
    {
        try
        {
            System.out.print("Accessing element number " + index);
            list.get(index);
            System.out.print(" does not trigger");
        }
        catch (IndexOutOfBoundsException exception)
        {
            System.out.print(" triggers");
        }
        finally
        {
            System.out.println(" IndexOutOfBoundsException");
        }
    }

    public static void checkArithmetic(int number)
    {
        try
        {
            System.out.print("Dividing " + number);
            number = 1 / number;
            System.out.print(" does not trigger");
        }
        catch (ArithmeticException exception)
        {
            System.out.print(" triggers");
        }
        finally
        {
            System.out.println(" ArithmeticException");
        }

    }

    public static void checkClassCast(Object obj)
    {
        try
        {
            System.out.print("Converting from " + obj.getClass() + " to Double");
            Double str = (Double) obj;
            System.out.print(" does not trigger");
        }
        catch (ClassCastException exception)
        {
            System.out.print(" triggers");
        }
        finally
        {
            System.out.println(" ClassCastException");
        }
    }

    public static void checkIO(String fileName)
    {
        try
        {
            System.out.print("Accessing \"" + fileName + "\"");
            FileReader reader = new FileReader(fileName);
            System.out.print(" does not trigger");
        }
        catch (IOException exception)
        {
            System.out.print(" triggers");
        }
        finally
        {
            System.out.println(" IOException");
        }
    }

    public static void checkFileNotFound(String fileName)
    {
        try
        {
            System.out.print("Opening \"" + fileName + "\"");  
            FileInputStream stream = new FileInputStream(fileName);
            System.out.print(" does not trigger");
        }
        catch (FileNotFoundException exception)
        {
            System.out.print(" triggers");
        }
        finally
        {
            System.out.println(" FileNotFoundExceptrion");
        }
    }

    public static void main(String[] args) {
        checkNullPointer(new ExceptionTest());
        checkNullPointer(null);
        System.out.println();

        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        checkIndexOutOfBounds(list, 0);
        checkIndexOutOfBounds(list, 1);
        System.out.println();

        checkArithmetic(1);
        checkArithmetic(0);
        System.out.println();

        checkClassCast(new Double(0));
        checkClassCast(new String());
        System.out.println();

        checkIO("a.txt");
        checkIO("b.txt");
        System.out.println();

        checkFileNotFound("a.txt");
        checkFileNotFound("b.txt");
        System.out.println();

    }   

}